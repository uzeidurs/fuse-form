var Observable = require('FuseJS/Observable');

var fields = Observable();
fields.add(new Field('f01','text','Field One'));
fields.add(new Field('f02','select','Field Two Rocks'));

function Field(id, type, label) {
	var self = this;
	this.id = id;
	this.type = type;
	this.label = label;
	this.content = Observable('');

	if (type == 'select') {
		this.options = getSelectOptions(id);
		this.selected = Observable();
		this.selected.onValueChanged(module, function(oid) {
			// console.log('selected changed to: ' + JSON.stringify(x));
			self.content.value = getOptionLabel(self.id, oid);
		});
	}
};

function getFieldData(fid) {
	var res = {};
	fields.forEach(function(item) {
		if (item.id == fid) res = item;
	});
	return res;
};

function getSelectOptions(fid) {
	var res = Observable();
	res.add({'id':'o01', 'label':'Option 1'});
	res.add({'id':'o02', 'label':'Option 2'});
	res.add({'id':'o03', 'label':'Option 3'});
	return res;
};

function getOptionLabel(fid, oid) {
	res = '';
	fields.forEach(function(item) {
		if (item.id == fid) {
			item.options.forEach(function(opt) {
				if (opt.id == oid) res = opt.label;
			});
		}
	});
	return res;
};

module.exports = {
	'fields': fields,
	'getFieldData': getFieldData
};
