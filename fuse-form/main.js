var Form = require('modules/Form');

function openField(args) {
	// console.log(JSON.stringify(args));
	// LOG: {"node":{"external_object":{}},"data":{"id":"f01","type":"text","label":"Field One"}}
	router.push('field', {'fieldId':args.data.id});
};

module.exports = {
	'fields': Form.fields,
	'openField': openField
};