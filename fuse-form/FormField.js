var Observable = require('FuseJS/Observable');
var Form = require('modules/Form');

var currentField = Observable();

this.Parameter.onValueChanged(module, function(param) {
	currentField.value = param.fieldId;
});

var data = currentField.map(function(fid) {
	return Form.getFieldData(fid);
});

function backToList() {
	router.goBack();
};

module.exports = {
	'data': data,
	'backToList': backToList
};